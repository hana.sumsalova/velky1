# Velka prace

## Zaciname
Toto je jeden z domacích úkolů z předmětu SKJ, který využívám pro velky projekt do predmetu SSP.

## Začínáme

- [Clone with SSH ](git@gitlab.com:hana.sumsalova/maly1.git) 
- [Clone with HTTPS ](https://gitlab.com/hana.sumsalova/maly1.git) 

## Jak se instaluje
Instalace s vzužitím virtuálního prostředí:

```
$ python3 -m venv venv_dir_path
$ source venv_dir_path/bin/activate
(venv) $ pip install pytest
(venv) $ python -m pytest tests.py
(venv) $ python -m pytest -v tests.py
(venv) $ python -m pytest -vv tests.py
```

## Jak se používá
1. V rámci virtuálního prostředí spusťte následující skript: python -m pytest tests.py
2. Skript Vám ukáže výsledky testů (test.py)